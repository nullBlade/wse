use gl::types::GLfloat;
use gl::types::GLsizei;
use gl::types::GLsizeiptr;
use std::ffi::c_void;
use std::mem;
use std::ptr::null;

pub struct Mesh {
    vao: u32,
    vbo: u32,
    ebo: u32,
    pub size: i32,
}

pub fn create_mesh(vertices: Vec<f32>, indices: Vec<i32>, attributes: Vec<i32>) -> Mesh {
    create_mesh_advanced(vertices, indices, attributes, gl::STATIC_DRAW)
}

pub fn create_mesh_dynamic(vertices: Vec<f32>, indices: Vec<i32>, attributes: Vec<i32>) -> Mesh {
    create_mesh_advanced(vertices, indices, attributes, gl::DYNAMIC_DRAW)
}

pub fn create_mesh_advanced(vertices: Vec<f32>, indices: Vec<i32>, attributes: Vec<i32>, option: gl::types::GLenum) -> Mesh {
    let mut vao: u32 = 0;
    let mut vbo: u32 = 0;
    let mut ebo: u32 = 0;
    let size = indices.len();
    unsafe {
        gl::GenVertexArrays(1, &mut vao);

        gl::GenBuffers(1, &mut vbo);
        gl::GenBuffers(1, &mut ebo);

        gl::BindVertexArray(vao);

        gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
        gl::BufferData(
            gl::ARRAY_BUFFER,
            (vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
            &vertices[0] as *const f32 as *const c_void,
            gl::STATIC_DRAW,
        );

        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ebo);
        gl::BufferData(
            gl::ELEMENT_ARRAY_BUFFER,
            (indices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
            &indices[0] as *const i32 as *const c_void,
            option,
        );

        let mut size = 0;
        for attrib in &attributes {
            size += attrib;
        }

        let mut i = 0;
        let mut pos = 0;
        for attrib in &attributes {
            gl::VertexAttribPointer(
                i,
                3,
                gl::FLOAT,
                gl::FALSE,
                (size as usize * mem::size_of::<f32>()) as GLsizei,
                (pos as usize * mem::size_of::<f32>()) as *const i32 as *const c_void,
            );

            gl::EnableVertexAttribArray(i);
            i += 1;
            pos += attrib;
        }

        gl::BindBuffer(gl::ARRAY_BUFFER, 0);

        gl::BindVertexArray(0);
    }

    Mesh {
        vao,
        vbo,
        ebo,
        size: size.try_into().unwrap(),
    }
}

impl Mesh {
    pub fn bind(&self) {
        unsafe {
            gl::BindVertexArray(self.vao);
        }
    }

    pub fn draw(&self) {
        unsafe {
            gl::DrawElements(gl::TRIANGLES, self.size, gl::UNSIGNED_INT, null());
        }
    }

    pub fn delete(&self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vao);
            gl::DeleteBuffers(1, &self.vbo);
            gl::DeleteBuffers(1, &self.ebo);
        }
    }

    pub fn re_build(&self, vertices: Vec<f32>, indices: Vec<i32>, attributes: Vec<i32>) {
        unsafe {

            gl::BindVertexArray(self.vao);

            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                &vertices[0] as *const f32 as *const c_void,
                gl::STATIC_DRAW,
            );

            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.ebo);
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (indices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                &indices[0] as *const i32 as *const c_void,
                gl::DYNAMIC_DRAW,
            );

            let mut size = 0;
            for attrib in &attributes {
                size += attrib;
            }

            let mut i = 0;
            let mut pos = 0;
            for attrib in &attributes {
                gl::VertexAttribPointer(
                    i,
                    3,
                    gl::FLOAT,
                    gl::FALSE,
                    (size as usize * mem::size_of::<f32>()) as GLsizei,
                    (pos as usize * mem::size_of::<f32>()) as *const i32 as *const c_void,
            );

                gl::EnableVertexAttribArray(i);
                i += 1;
                pos += attrib;
            }

            gl::BindBuffer(gl::ARRAY_BUFFER, 0);

            gl::BindVertexArray(0);
        }
    }
}
