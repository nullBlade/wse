use crate::engine::engine::Engine;
use crate::engine::renderer::ui::ui::{new_ui_manager, UI, UIManager};
use cgmath::{perspective, Deg, Matrix4, Vector3};
use std::sync::mpsc::Receiver;
use glfw::{Action, MouseButton};

pub struct Renderer {
    projection: Matrix4<f32>,
    view: Matrix4<f32>,

    ui_manager: UIManager,

    // true mouse window size is used to divide mouse position to get mouse position in terms of a float.
    pub true_mouse_window_size: (i32, i32),

    //
    pub map_model_change_que: Option<Receiver<MapModelChangeEnum>>,

    pub mouse_locked: bool
}

pub fn init() -> Renderer {

    let projection = perspective(Deg(45.0), 10 as f32 / 10 as f32, 0.1, 100.0);
    let view = Matrix4::from_translation(Vector3 {
        x: 0.,
        y: 0.,
        z: 0.,
    });

    Renderer {
        projection,
        view,

        ui_manager: new_ui_manager(),

        true_mouse_window_size: (0, 0),

        map_model_change_que: None,

        mouse_locked: false
    }
}

pub enum MapModelChangeEnum {

}

impl Renderer {
    // the render function
    pub fn render(&self, engine: &Engine) {
        // parse the map model change que if it exists and it has anything
        match &self.map_model_change_que {
            Some(change) => {
                match change {

                    _ => {}
                }
            }
            _ => {}
        }



        // call ui manager to render ui
        self.ui_manager.render();
    }

    /// starts rendering the ui
    pub fn add_ui (&mut self, ui: UI) {
        self.ui_manager.uis.push(ui);
    }

    // sets mouse pos for ui
    pub fn set_mouse_pos (&mut self, x: f64, y: f64) {
        // divide by screen resolution to get coordinate 0-1 for both x and y
        // then we multiply by 2 and minus 1 to get coord -1-1
        // minus y from 0 to mirror it so that it went up
        self.ui_manager.values.mouse_position = (x as f32 / self.true_mouse_window_size.0 as f32 * 2.0 - 1.0,
                                          -(y as f32 / self.true_mouse_window_size.1 as f32 * 2.0 - 1.0));


    }

    // update mouse
    pub fn update_mouse_clicks(&mut self, action: Action, mouse_button: MouseButton) {

        if self.mouse_locked { // return if mouse is locked to decrease useless actions in case the player's mouse is locked
            return;
        }

        self.ui_manager.on_click_action(action, mouse_button);
        match mouse_button {
            MouseButton::Button1 => {
                match action {
                    Action::Release => {
                        self.ui_manager.values.first_mouse_button_pressed = false;

                    }
                    Action::Press => {
                        self.ui_manager.values.first_mouse_button_pressed = true;
                    }
                    Action::Repeat => {} // I don't think repeat will be useful here?
                }
            }
            MouseButton::Button2 => {}
            MouseButton::Button3 => {}
            MouseButton::Button4 => {}
            MouseButton::Button5 => {}
            MouseButton::Button6 => {}
            MouseButton::Button7 => {}
            MouseButton::Button8 => {}
        }
    }

    pub fn set_window_size(&mut self, size: (i32, i32)) {
        self.true_mouse_window_size = size.clone();
    }

    pub fn set_framebuffer_size(&mut self, size: (i32, i32)) {
        self.ui_manager.values.screen_size = size.clone();
        self.projection = perspective(Deg(90.0), size.0 as f32 / size.1 as f32, 0.1, 100.0);
    }
}
