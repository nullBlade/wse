pub mod mesh;
pub mod renderer;
pub mod shader;
pub mod texture;

// ui itself |consider moving if there is too many lines|
pub mod ui {
    pub mod text_renderer;
    pub mod ui;

    // ui elements
    pub mod elements {
        pub mod button;
        pub mod panel;
    }
}
