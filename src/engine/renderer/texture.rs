use image::load_from_memory;
use std::ffi::c_void;

pub struct Texture {
    texture: u32,
}

pub fn make_texture(image: &[u8]) -> Texture {
    let img = load_from_memory(image)
        .expect("Failed to load texture")
        .to_rgb8();

    let mut texture = 0;
    unsafe {
        gl::GenTextures(1, &mut texture);
        gl::BindTexture(gl::TEXTURE_2D, texture);

        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT as i32);

        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);

        let (w, h) = (img.width(), img.height());
        let data = img.as_raw();
        gl::TexImage2D(
            gl::TEXTURE_2D,
            0,
            gl::RGB as i32,
            w as i32,
            h as i32,
            0,
            gl::RGB,
            gl::UNSIGNED_BYTE,
            &data[0] as *const u8 as *const c_void,
        );
        gl::GenerateMipmap(gl::TEXTURE_2D);
    }

    Texture { texture }
}

pub fn bind_texture_id(id: u32, n: u32) {
    unsafe {
        gl::ActiveTexture(gl::TEXTURE0 + n);
        gl::BindTexture(gl::TEXTURE_2D, id);
    }
}

impl Texture {
    pub fn bind(&self, n: u32) {
        unsafe {
            gl::ActiveTexture(gl::TEXTURE0 + n);
            gl::BindTexture(gl::TEXTURE_2D, self.texture);
        }
    }

    pub fn clean_up(&self) {
        unsafe {
            gl::DeleteTextures(1, &self.texture);
        }
    }

    pub fn to_texture_id(&self) -> u32 {
        return self.texture
    }
}
