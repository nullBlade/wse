use crate::engine::renderer::mesh::Mesh;
use crate::engine::renderer::shader::Shader;
use crate::engine::renderer::{mesh, shader};
use std::ffi::CString;
use glfw::{Action, MouseButton};

// I am sorry for this very unorganised file but here is some order help
// static setting values
// UI manager with it's new method thing
// UI struct with it's new method
// UI functions
// UI position
// UIElement trait

// if you need to edit this file, please talk to me
// by 100% sane nullBlade

// todo: break in to multiple files

// ui scale
static SCALE: f32 = 1.0;

// for managing data needed for ui like meshes and shaders
pub struct UIManager {
    // list of uis to draw
    pub uis: Vec<UI>,

    pub values: UIManagerValues // contain values here to later pass to mutable ui elements
}

pub struct UIManagerValues {
    // ui panel mesh
    pub(crate) panel_mesh: Mesh,

    // ui panel shader
    pub(crate) panel_shader: Shader,
    pub(crate) panel_shader_pos_uniform: i32,

    // screen size
    pub screen_size: (i32, i32),

    // mouse position
    pub mouse_position: (f32, f32),

    // is mouse clicked
    pub first_mouse_button_pressed: bool
}

pub fn new_ui_manager() -> UIManager {

    let panel_shader = shader::create_shader( // include needed shaders
        include_str!("../shaders/panel_vertex.glsl"),
        include_str!("../shaders/panel_fragment.glsl"),
    );
    let panel_shader_pos_uniform = panel_shader.get_uniform(CString::new("pos").unwrap());



    return UIManager {
        // list of currently renderer uis
        uis: Vec::new(),

        values: UIManagerValues {
            panel_mesh: mesh::create_mesh(
                vec![
                                    1.0, 0.0, 1.0, 0.0,
                                    1.0, 1.0, 1.0, 1.0,
                                    0.0, 1.0, 0.0, 1.0,
                                    0.0, 0.0, 0.0, 0.0,
                ],
                vec![0, 1, 3, 1, 2, 3],
                vec![2, 2],
            ),

            panel_shader,
            panel_shader_pos_uniform,

            screen_size: (10, 10), // set screen size which will be overwritten soon
            mouse_position: (100000.0, 0.0), // set mouse position to be off screen
            first_mouse_button_pressed: false // set it to false so that it was, well false
        }
    };
}

impl UIManager {
    // render method
    pub fn render(&self) {
        for ui in &self.uis {
            ui.draw(&self);
        }
    }

    // button press
    pub fn on_click_action (&mut self, action: Action, mouse_button: MouseButton) {
        for ui in &mut self.uis {
            ui.on_click_action(&self.values, action, mouse_button);
        }
    }
}

// ah flip here we go again
pub struct UI {
    pub(crate) name: String,
    pub(crate) elements: Vec<Box<dyn UIElement>>,

}

// ui drawing code is written here
impl UI {
    // add element
    pub fn add_element(&mut self, element: Box<dyn UIElement>) {
        self.elements.push( element);
    }

    // the click function
    pub fn on_click_action(&mut self, values: &UIManagerValues, action: Action, mouse_button: MouseButton) {
        // iterate
        for element in &mut self.elements {
            // call the click function
            element.click(values, action, mouse_button);
        }
    }

    // the update/draw function
    pub fn draw(&self, manager: &UIManager) {
        // iterate
        for element in &self.elements {
            // draw
            element.draw(&manager.values);
        }
    }
}

pub enum UIPosition {
    /// x, y, size x, size y
    CenterMenu(f32, f32, f32, f32),
}

// position code is written here.
impl UIPosition {
    pub fn get_position(&self, scale: (i32, i32)) -> (f32, f32, f32, f32) {

        // create x and y multipliers by checking which one smaller
        let (xm, ym) = if scale.0 > scale.1 {
            (scale.1 as f32 / scale.0 as f32, 1.0)
        } else {
            (1.0, scale.0 as f32 / scale.1 as f32)
        };

        // match the ui positions
        match self {
            UIPosition::CenterMenu(x, y, xs, ys) => (
                ((x - xs / 2.0) * SCALE * xm),
                ((y - ys / 2.0) * SCALE * ym),
                xs * SCALE * xm,
                ys * SCALE * ym,
            ),
        }
    }
}

// ui element
// pub enum UIElement {
//     Panel(Panel),
//     Button(Button)
// }

// ui element trait
pub trait UIElement {
    fn draw(&self, values: &UIManagerValues);
    fn click(&mut self, values: &UIManagerValues, action: Action, button: MouseButton);
}