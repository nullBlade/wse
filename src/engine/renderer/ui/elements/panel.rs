use glfw::{Action, MouseButton};
use crate::texture::{bind_texture_id, Texture};
use crate::ui::{UIElement, UIManagerValues};
use crate::{UIPosition};

// panel
pub struct Panel {
    pub pos: UIPosition,
    /// opengl texture id
    pub texture: u32
}

// new method
pub fn new (position: UIPosition, texture: &Texture) -> Panel {
    return Panel {
        pos: position,
        texture: texture.to_texture_id(),
    };
}

impl UIElement for Panel {
    fn draw (&self, values: &UIManagerValues) {
        // bind the panel textures
        bind_texture_id(self.texture, 0);

        // turn panel in to position and size
        let (x, y, xs, ys) = self.pos.get_position(values.screen_size);

        // use the panel shader
        values.panel_shader.use_shader();

        // this is unsafe
        unsafe {
            // set vector4 uniform pos to pos
            gl::Uniform4f(
                values.panel_shader_pos_uniform,
                x,
                y,
                xs,
                ys
            );
        }

        // bind and draw panel mesh
        values.panel_mesh.bind();
        values.panel_mesh.draw();
    }

    fn click(&mut self, _values: &UIManagerValues, _action: Action, _button: MouseButton) {
        // nothing
    }
}