use glfw::{Action, MouseButton};
use crate::texture::{bind_texture_id, Texture};
use crate::ui::{UIElement, UIManagerValues};
use crate::UIPosition;

// button
pub struct Button {
    pub pos: UIPosition,
    /// opengl texture id,
    /// 0: normal texture
    /// 1: hovered texture
    /// 2: pressed texture
    pub texture: [u32; 3],

    pub click_action: fn (),

    pressed: bool // to check if the button was held before being pressed.
}

// new method
pub fn new (position: UIPosition, texture_normal: &Texture, texture_hovered: &Texture, texture_pressed: &Texture, click_action: fn()) -> Button {
    return Button {
        pos: position,
        texture: [texture_normal.to_texture_id(), texture_hovered.to_texture_id(), texture_pressed.to_texture_id()],
        click_action,
        pressed: false
    };
}

impl UIElement for Button {
    fn draw (&self, values: &UIManagerValues) {
        // turn button in to position and size
        let (x, y, xs, ys) = self.pos.get_position(values.screen_size);

        // bind the button textures after checking it's state (if it is pressed/hovered over)

        if values.mouse_position.0 > x && values.mouse_position.0 < x + xs &&
            values.mouse_position.1 > y && values.mouse_position.1 < y + ys { // checking if mouse is on top of button
            if self.pressed {
                bind_texture_id(self.texture[2], 0);
            } else if !values.first_mouse_button_pressed {
                bind_texture_id(self.texture[1], 0);
            }
        } else {
            bind_texture_id(self.texture[0], 0);
        }

        // use the panel shader
        values.panel_shader.use_shader();

        // this is unsafe
        unsafe {
            // set vector4 uniform pos to pos
            gl::Uniform4f(
                values.panel_shader_pos_uniform,
                x,
                y,
                xs,
                ys
            );
        }

        // bind and draw panel mesh
        values.panel_mesh.bind();
        values.panel_mesh.draw();
    }
    fn click (&mut self, values: &UIManagerValues, action: Action, button: MouseButton) {

        // check if it is the needed click effect
        match action {
            Action::Release => {
                // check if the button was pressed before otherwise return
                if !self.pressed {
                    return;
                }

                match button {
                    MouseButton::Button1 => {
                        // change pressed to false
                        self.pressed = false;

                        // get position
                        let (x, y, xs, ys) = self.pos.get_position(values.screen_size);

                        // make sure the mouse is on the button
                        if values.mouse_position.0 > x && values.mouse_position.0 < x + xs &&
                            values.mouse_position.1 > y && values.mouse_position.1 < y + ys {

                            // call the click action
                            (self.click_action)();

                        }
                    }
                    _ => {}
                }
            }
            Action::Press => {
                match button {
                    MouseButton::Button1 => {
                        // get position
                        let (x, y, xs, ys) = self.pos.get_position(values.screen_size);

                        // make sure the mouse is on the button
                        if values.mouse_position.0 > x && values.mouse_position.0 < x + xs &&
                            values.mouse_position.1 > y && values.mouse_position.1 < y + ys {

                            // change that the button is pressed
                            self.pressed = true;
                        }
                    }
                    _ => {}
                }
            }

            _ => {}
        }
    }
}