use gl::types::GLchar;
use gl::types::GLint;
use std::ffi::CString;

pub struct Shader {
    program: u32,
}

pub fn create_shader(vertex_shader_code: &str, fragment_shader_code: &str) -> Shader {
    let program = unsafe {
        let vertex_shader = gl::CreateShader(gl::VERTEX_SHADER);
        let vertex_shader_code = CString::new(vertex_shader_code).unwrap();
        gl::ShaderSource(
            vertex_shader,
            1,
            &(vertex_shader_code.as_ref()).as_ptr(),
            std::ptr::null(),
        );
        gl::CompileShader(vertex_shader);

        let mut success = gl::FALSE as GLint;
        gl::GetShaderiv(vertex_shader, gl::COMPILE_STATUS, &mut success);
        if success != gl::TRUE as GLint {
            let mut info_log = [0 ; 512];
            gl::GetShaderInfoLog(
                vertex_shader,
                512,
                std::ptr::null_mut(),
                info_log.as_mut_ptr() as *mut GLchar,
            );
            println!(
                "Vertex shader compilation failed \n{}",
                std::str::from_utf8(&info_log).unwrap()
            );
        }

        let fragment_shader = gl::CreateShader(gl::FRAGMENT_SHADER);
        let fragment_shader_code = CString::new(fragment_shader_code).unwrap();
        gl::ShaderSource(
            fragment_shader,
            1,
            &(fragment_shader_code.as_ref()).as_ptr(),
            std::ptr::null(),
        );
        gl::CompileShader(fragment_shader);

        let mut success = gl::FALSE as GLint;
        gl::GetShaderiv(fragment_shader, gl::COMPILE_STATUS, &mut success);
        if success != gl::TRUE as GLint {
            let mut info_log = [0 ; 512];
            gl::GetShaderInfoLog(
                fragment_shader,
                512,
                std::ptr::null_mut(),
                info_log.as_mut_ptr() as *mut GLchar,
            );
            println!(
                "Vertex shader compilation failed \n{}",
                std::str::from_utf8(&info_log).unwrap()
            );
        }

        let program = gl::CreateProgram();

        gl::AttachShader(program, vertex_shader);
        gl::AttachShader(program, fragment_shader);
        gl::LinkProgram(program);

        let mut success = gl::FALSE as GLint;
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut success);
        if success != gl::TRUE as GLint {
            let mut info_log = [0 ; 512];
            gl::GetProgramInfoLog(
                program,
                512,
                std::ptr::null_mut(),
                info_log.as_mut_ptr() as *mut GLchar,
            );
            println!(
                "Shader linking failed \n{}",
                std::str::from_utf8(&info_log).unwrap()
            );
        }

        gl::DeleteShader(vertex_shader);
        gl::DeleteShader(fragment_shader);

        program
    };

    Shader { program }
}

impl Shader {
    pub fn use_shader(&self) {
        unsafe {
            gl::UseProgram(self.program);
        }
    }

    pub fn delete_shader(&self) {
        unsafe {
            gl::DeleteProgram(self.program);
        }
    }

    pub fn get_uniform(&self, uniform_name: CString) -> i32 {
        unsafe {
            let uniform = gl::GetUniformLocation(self.program, uniform_name.as_ptr());

            if uniform == -1 {
                println!("Can not find a uniform.");
            }
            uniform
        }
    }
}