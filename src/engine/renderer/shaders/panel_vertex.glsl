#version 330 core

layout (location=0) in vec2 position;
layout (location=1) in vec2 texCoord;

out vec2 TexCoord;

uniform vec4 pos;

void main()
{
    gl_Position = vec4(pos.x + position.x * pos.z, pos.y + position.y * pos.w, 0, 1.0);
    TexCoord = vec2(texCoord.x, 1.0 - texCoord.y);
}