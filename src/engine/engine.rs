extern crate glfw;

use crate::engine::renderer;
use crate::engine::renderer::renderer::Renderer;
use gl;
use glfw::{Action, Context, Glfw, Key, OpenGlProfileHint, Window, WindowEvent, WindowHint};
use std::sync::mpsc::Receiver;
use std::time::SystemTime;

pub struct Engine {
    window: Window,
    glfw: Glfw,
    events: Receiver<(f64, WindowEvent)>,
    //pub window_size: (i32, i32),
    pub renderer: Renderer,
    pub start_time: SystemTime,
}

pub fn init(name: &str) -> Engine {
    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

    glfw.window_hint(WindowHint::ContextVersion(3, 3));
    glfw.window_hint(WindowHint::OpenGlProfile(OpenGlProfileHint::Core));

    #[cfg(target_os = "macos")]
    glfw.window_hint(WindowHint::OpenGlForwardCompat(true));

    let (mut window, events) = glfw
        .create_window(900, 700, name, glfw::WindowMode::Windowed)
        .expect("Failed to create GLFW window.");

    window.make_current();
    window.set_key_polling(true);
    window.set_cursor_pos_polling(true);
    window.set_size_polling(true);
    window.set_framebuffer_size_polling(true);
    window.set_mouse_button_polling(true);

    gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);

    unsafe {
        gl::Enable(gl::DEPTH_TEST);
        gl::Enable(gl::CULL_FACE);
    }

    let renderer = renderer::renderer::init();

    Engine {
        window,
        glfw,
        events,
        //window_size: (900, 700),
        renderer,
        start_time: SystemTime::now()
    }
}

impl Engine {
    pub fn start_loops(&mut self) {
        // set window size for use in rendering ui and also creating projection and view matrices
        self.renderer.set_window_size(self.window.get_size());
        self.renderer.set_framebuffer_size(self.window.get_framebuffer_size());

        while !self.window.should_close() {
            self.glfw.poll_events();
            for (_, event) in glfw::flush_messages(&self.events) {
                match event {
                    WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
                        self.window.set_should_close(true)
                    }
                    WindowEvent::Size(x, y) => {
                        //self.window_size = (x, y);
                        self.renderer.set_window_size((x, y));
                    }
                    WindowEvent::FramebufferSize(x, y) => {
                        unsafe {
                            gl::Viewport(0, 0, x, y);
                        }

                        self.renderer.set_framebuffer_size((x, y));
                    }
                    WindowEvent::CursorPos(x, y) => {
                        self.renderer.set_mouse_pos(x, y);
                    }
                    WindowEvent::MouseButton(button, action, _modifiers) => {
                        self.renderer.update_mouse_clicks(action, button);
                    }
                    _ => {}
                }
            }

            unsafe {
                gl::ClearColor(0.2, 0.6, 1.0, 1.0);
                gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
            }

            self.renderer.render(&self);

            self.window.swap_buffers();
        }
    }
}
