use crate::engine::renderer::texture;
use crate::engine::renderer::ui::elements::button;
use crate::engine::renderer::ui::ui;
use crate::ui::{UI, UIElement, UIPosition};

pub mod engine;

fn main() {
    let mut engine = engine::engine::init("Test");

    let mut buttons: Vec<Box<dyn UIElement>> = Vec::new();
    let n = texture::make_texture(include_bytes!("test_resources/frame.png"));
    let h = texture::make_texture(include_bytes!("test_resources/frame_selected.png"));
    let p = texture::make_texture(include_bytes!("test_resources/frame_pressed.png"));
    for x in -15..15 {
        for y in -15..15 {
            buttons.push(Box::new(button::new(UIPosition::CenterMenu(x as f32 / 10.0, y as f32 / 10.0, 0.1, 0.1), &n, &h, &p,
            || {
                println!("You pressed a button");
            })));
        }
    }

    engine.renderer.add_ui(UI {
        name: "hmm".to_string(),
        elements: buttons
    });

    engine.start_loops();
}
